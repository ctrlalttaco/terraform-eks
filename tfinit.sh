#!/usr/bin/env bash

terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/34486195/terraform/state/terraform-sandbox" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/34486195/terraform/state/terraform-sandbox/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/34486195/terraform/state/terraform-sandbox/lock" \
    -backend-config="username=$GITLAB_USERNAME" \
    -backend-config="password=$GITLAB_ACCESS_TOKEN" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"
